import java.util.Calendar;

class Sixth{
    private static final int DAYS = 7;

    public static void main(String[] args){
        System.out.println("Kaspersky will solve this task in "+DAYS+" days");

        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, DAYS);
        int day = now.get(Calendar.DAY_OF_MONTH);
        int month = now.get(Calendar.MONTH);
        int year = now.get(Calendar.YEAR);
        System.out.println("Work will be done on "+day+"."+month+"."+year);
    }
}