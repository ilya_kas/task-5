import java.util.Random;

class Fifth{
    public static void main(String[] args){
        int sum = 0;
        int product = (args.length>0)?1:0;

        for (int i=0; i<args.length; i++){
            int x;
            try {
                x = Integer.parseInt(args[i]);
            }catch (NumberFormatException e){
                System.out.println("Arguments must be integers");
                return;
            }
            sum += x;
            product *= x;
        }
        System.out.println(sum);
        System.out.println(product);
    }
}