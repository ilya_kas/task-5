import java.util.Random;

class Fourth{
	private static final String PASSWORD = "password";

    public static void main(String[] args){
        if (args.length != 1){
            System.out.println("Wrong args count");
            return;
        }

        if (args[0].equals(PASSWORD))
            System.out.println("Correct");
        else
            System.out.println("Wrong password");
    }
}