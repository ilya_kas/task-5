import java.util.Random;

class Third{
    private static final int MAX = 100;
    public static void main(String[] args){
        if (args.length != 1){
            System.out.println("Wrong args count");
            return;
        }
        Random random = new Random();

        int n;
        try {
            n = Integer.parseInt(args[0]);
        }catch (NumberFormatException e){
            System.out.println("Argument must be an integer");
            return;
        }
        int[] numbers = new int[n];
        for (int i=0; i<n; i++)
            numbers[i] = random.nextInt() % MAX;

        for (int i=0; i<n; i++)
            System.out.print(numbers[i]+" ");
        System.out.println();
        for (int i=0; i<n; i++)
            System.out.println(numbers[i]+" ");
    }
}